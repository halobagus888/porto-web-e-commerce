<?php

use App\Http\Controllers\Api\OrderItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('order')->group(function () 
    {
        //item
        Route::get('/{order_id}', [OrderItemController::class, 'index']);
        Route::post('/{order_id}', [OrderItemController::class, 'store']);
        Route::get('/{order_id}/orderitems/edititem/{id}', [OrderItemController::class, 'edit']);
        Route::post('/{order_id}/orderitems/edititem/{id}', [OrderItemController::class, 'update']);
        Route::get('/orderitems/search/{id}', [OrderItemController::class, 'search']);
        Route::delete('/{order_id}/orderitems/delete/{id}', [OrderItemController::class, 'destroy']);
    });
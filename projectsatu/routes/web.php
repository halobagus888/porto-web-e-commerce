<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserSearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['middleware' => 'is_admin']);


Route::get('/admin', [AdminController::class, 'dashboard']);
// Route::post('admin', [ 'as' => 'admin', 'uses' => 'adminController@do']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'middleware' => 'is_admin'], function () {
    // route user
    Route::prefix('user')->group(function () 
    {
        Route::get('/', [AdminController::class, 'ulist']);
        //route  insert
        Route::get('/insert', [AdminController::class, 'uinsert']);
        Route::post('/insert', [AdminController::class, 'uinsertkirim']);
        Route::get('/hapus/{id}', [AdminController::class, 'uhapus']);
        
    });

    //route product
    Route::prefix('product')->group(function () 
    {
        Route::get('/', [AdminController::class, 'plist']);
        //route  insert
        Route::get('/insert', [AdminController::class, 'pinsert']);
        Route::post('/insert', [AdminController::class, 'pinsertkirim']);
        Route::get('/edit/{id}', [AdminController::class, 'pedit']);
        Route::post('/edit/{id}', [AdminController::class, 'peditkirim']);
        Route::get('/hapus/{id}', [AdminController::class, 'phapus']);
    });

    //route order
    Route::prefix('order')->group(function () 
    {
        Route::get('/', [AdminController::class, 'order_list']);
        //route order
        Route::get('/insert', [AdminController::class, 'oinsert']);
        Route::post('/insert', [AdminController::class, 'oinsertkirim']);
        Route::get('/edit/{id}', [AdminController::class, 'oedit']);
        Route::post('/edit/{id}', [AdminController::class, 'oeditkirim']);
        Route::get('/hapus/{id}', [AdminController::class, 'ohapus']);
        //item
        Route::get('/{order_id}', [AdminController::class, 'item_list']);
        Route::get('/{order_id}/insertitem', [AdminController::class, 'iinsert']);
        Route::post('/{order_id}/insertitem', [AdminController::class, 'iinsertkirim']);
        Route::get('/{item_id}/edititem', [AdminController::class, 'iedit']);
        Route::post('/{item_id}/edititem', [AdminController::class, 'ieditkirim']);
        Route::get('/{item_id}/hapus', [AdminController::class, 'ihapus']);
    });
});

Route::get('/', [UserController::class, 'index']);
Route::get('/cart', [UserController::class, 'cart']);
Route::get('/category', [UserController::class, 'category']);
Route::get('/checkout', [UserController::class, 'checkout']);
Route::get('/contact', [UserController::class, 'contact']);
Route::get('/product', [UserController::class, 'product']);
// Route::get('/search', [UserSearchController::class, 'index']);


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

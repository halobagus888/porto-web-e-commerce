<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;


class AdminController extends Controller
{
    // public function index()
    // {
    //     return view('index');
    // }
    public function dashboard()
    {
        return view('admin/master');
    }
// user
    public function ulist()
    {
        $datauser = User::orderBy('id','DESC')->paginate(10);
        return view('admin/user/ulist',['ulists'=>$datauser]);
    }

    // insert
    public function uinsert()
    {
        return view('admin.user.insert_ulist');  
    }

    public function uinsertkirim(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string',
            'email' => 'email',
            'password' => 'required'
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = new User;
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->save();


        return back()->with('message','User baru berhasil ditambahkan');
    }

    public function uhapus($id) 
    {
        $phapus = User::find($id);
        $phapus->delete();

        return back()->with('message','Berhasil Dihapus');
    }



// produk
    public function plist()
    {
        $dataprodukk = Product::orderBy('id','DESC')->paginate(10);
        return view('admin/product/plist', ['plists'=>$dataprodukk]);
    }

    // insert
    public function pinsert()
    {
        $categories = Category::get();
        return view('admin.product.insert_plist',compact('categories'));  
    }

    public function pinsertkirim(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'code' => 'numeric|unique:products',
            'name' => 'string',
            'stock' => 'numeric|integer',
            'varian' => 'string',
            'image' => 'string',
            'category_id' => 'numeric',
        ]);


        $code = $request->input('code');
        $name = $request->input('name');
        $stock = $request->input('stock');
        $varian = $request->input('varian');
        $image = $request->input('image');
        // $extension = $image->getClientOriginalExtension();
        // $originalname = $image->getClientOriginalName();
        // $path = $image->storeAs('', $originalname);
        // $mimetype = $image->getClientMimeType();
        $category_id = $request->input('category_id');

        $product = new Product;
        $product->code = $code;
        $product->name = $name;
        $product->stock = $stock;
        $product->varian = $varian;
        $product->image = $image;
        // $image->move(public_path().'/img/product', $image);
        $product->category_id = $category_id;
        $product->save();


        return back()->with('message','Berhasil Ditambahkan');
    }

    public function pedit($id)
    {
        $categories = Category::get();
        $dataprodukk = Product::find($id);
        return view('admin.product.edit_plist', compact('categories','dataprodukk'));
    }

    public function peditkirim(Request $request, $id)
    {
        $dataprodukk = Product::find($id);

        $validatedData = $request->validate([
            'code' => "numeric|unique:products,code,$id",
            'stock' => 'numeric|integer',
            'varian' => 'string',
            'category_id' => 'numeric',
        ]);

        $code = $request->input('code');
        $name = $request->input('name');
        $stock = $request->input('stock');
        $varian = $request->input('varian');
        $category_id = $request->input('category_id');

        $dataprodukk->id == $id;
        $dataprodukk->code = $code;
        $dataprodukk->name = $name;
        $dataprodukk->stock = $stock;
        $dataprodukk->varian = $varian;
        $dataprodukk->category_id = $category_id;
        $dataprodukk->save();

        return back()->with('message', 'Data Berhasil Dirubah');
    }

    public function phapus($id) 
    {
        $phapus = Product::find($id);
        $phapus->delete();

        return back()->with('message','Berhasil Dihapus');
    }

//order
    public function order_list()
    {
        $olist = Order::orderBy('id','DESC')->paginate(5);
        return view('admin/order/order_list', ['order_lists'=>$olist]);
    }

    public function oinsert()
    {
        $ulists = User::get();
        return view('admin.order.insert_order',compact('ulists'));  
    }

    public function oinsertkirim(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'tanggal_order' => 'required',
        ]);


        $user_id = $request->input('user_id');
        $tanggal_order = $request->input('tanggal_order');

        $order = new Order;
        $order->user_id = $user_id;
        $order->tanggal_order = $tanggal_order;
        $order->save();

        $id = $order->id;

        return back()->with('message','Berhasil Ditambahkan');
    }

    public function oedit($id)
    {
        $ulists = User::get();
        $olist = Order::find($id);
        return view('admin.order.edit_order', compact('ulists','olist'));
    }

    public function oeditkirim(Request $request, $id)
    {
        $olist = Order::find($id);

        $validatedData = $request->validate([
            'user_id' => 'required',
            'tanggal_order' => 'required',
        ]);


        $user_id = $request->input('user_id');
        $tanggal_order = $request->input('tanggal_order');

        $olist->id == $id;
        $olist->user_id = $user_id;
        $olist->tanggal_order = $tanggal_order;
        $olist->save();

        return back()->with('message', 'Data Berhasil Dirubah');
    }

    public function ohapus($id) 
    {
        $ohapus = Order::find($id);
        $ohapus->delete();

        return back()->with('message','Berhasil Dihapus');
    }

//items
    public function item_list($order_id)
    {
        $ulists = User::get();
        $olist = Order::get();
        $ilist = OrderItem::where('order_id', $order_id)->get();
        $dataprodukk = Product::get();
        return view('admin/order/item_list', compact('ulists', 'olist', 'order_id', 'ilist', 'dataprodukk'));
    }

    public function iinsert()
    {
        $ulists = User::get();
        $olist = Order::get();
        $dataprodukk = Product::get();
        return view('admin.order.insert_item',compact('ulists','olist','dataprodukk'));  
    }

    public function iinsertkirim(Request $request, $id)
    {
        $validatedData = $request->validate([
            'order_id' => 'required',
            'product_id' => 'required',
            'qty' => 'required',
        ]);

        return $request;

        
        return back()->with('message','Berhasil Ditambahkan');
    }
    
    public function iedit($id)
    {
        $ulists = User::get();
        $dataprodukk = Product::get();
        $ilist = OrderItem::find($id);
        return view('admin.order.edit_item', compact('ulists','dataprodukk','ilist'));
    }

    public function ieditkirim(Request $request, $id)
    {
        $ilist = OrderItem::find($id);

        $validatedData = $request->validate([
            'product_id' => 'required',
            'qty' => 'required',
        ]);


        $product_id = $request->input('product_id');
        $qty = $request->input('qty');

        $ilist->id == $id;
        $ilist->product_id = $product_id;
        $ilist->qty = $qty;
        $ilist->save();

        return back()->with('message', 'Data Berhasil Dirubah');
    }

    public function ihapus($id) 
    {
        $ihapus = OrderItem::find($id);
        $ihapus->delete();

        return back()->with('message','Berhasil Dihapus');
    }
}

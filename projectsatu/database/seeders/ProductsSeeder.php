<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        foreach(range(1,100) as $index){
            DB::table('products')->insert([
                'code' => $faker->unique()->randomNumber,
                'name' => $faker->name,
                'stock' => $faker->randomDigit,
                'varian' => $faker->sentence,
                'image' => $faker->imageUrl($width = 200, $height = 200),
                'description' => $faker->paragraph,
                'category_id' => rand(1,5)
            ]);
        }
    }
}

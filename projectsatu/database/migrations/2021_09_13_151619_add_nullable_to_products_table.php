<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable()->change();
            $table->text('code')->nullable()->change();
            $table->integer('stock')->nullable()->change();
            $table->text('varian')->nullable()->change();
            $table->longText('name')->nullable()->change();
            $table->text('image')->nullable()->change();
            $table->longText('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable(false)->change();
            $table->text('code')->nullable(false)->change();
            $table->integer('stock')->nullable(false)->change();
            $table->text('varian')->nullable(false)->change();
            $table->longText('name')->nullable(false)->change();
            $table->text('image')->nullable(false)->change();
            $table->longText('description')->nullable(false)->change();
        });
    }
}

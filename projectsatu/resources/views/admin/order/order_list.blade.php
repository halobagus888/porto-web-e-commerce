@extends('admin/master')

@section('content')
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
         @foreach ($errors->all() as $error)
             <li>{{$error}}</li>
         @endforeach
      </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Order</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a style="margin-bottom: 15px;" href="{{url('admin/order/insert')}}" class="btn btn-primary">Insert Order</a>
                <table id="example2" class="table table-bordered table-hover">
                  <tr>
                    <th style="text-align: center;">NAMA</th>
                    <th style="text-align: center;">TANGGAL ORDER</th>
                    <th style="text-align: center;">MENU</th>
                  </tr>
                  @foreach($order_lists as $row=>$order)
                    <tr>
                      <td><?php echo $order->user->name; ?></td>
                      <td><?php echo $order->tanggal_order; ?></td>
                      <td style="text-align: center;">
                        <a style="margin-right: 20px;" class="btn btn-xs btn-primary" href="{{url('admin/order/'.$order->id)}}">Check Order</a>
                        <a style="margin-right: 20px;" class="btn btn-xs btn-primary" href="{{url('admin/order/edit/'.$order->id)}}">Edit</a>
                        <a style="margin-right: 20px;" href="{{url('admin/order/hapus/'.$order->id)}}" class="btn btn-xs btn-danger" onclick="return confirm('Yakin dihapus?');" >Delete</a>
                      </td>
                    </tr>
                  @endforeach
                </table>
                <span>
                  {{ $order_lists->links() }}
                </span>
                <style>
                  .w-5{display: none;}
                </style>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
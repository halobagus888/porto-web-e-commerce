@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/order/'.$ilist->id.'/edititem')}}">
	@csrf

	<div class="card-body">
		<div class="form-group">
		<label for="varian">Nama Produk</label><br>
			<select class="form-control" name="product_id">
				<option value="{{$ilist->product_id}}">{{$ilist->product->varian}} - terpilih</option>
			@foreach ( $dataprodukk as $row )
				<option value="{{$row->id}}" {{ (old('row') == $row->name) ? 'selected' : ''}}>{{$row->name}} - {{$row->varian}}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="qty">Jumlah</label>
				<input class="form-control" placeholder="Jumlah Item" type="text" name="qty" value="{{old('qty',$ilist->qty)}}">
				@error('qty')
		        	<div class="invalid-feedback">{{ $message }}</div>
		        @enderror
		</div><br>
		
	    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</div>
<!-- /.card-body -->
</form>

@endsection
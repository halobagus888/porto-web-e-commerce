@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/order/insert')}}">
	@csrf

	<div class="card-body">
		<div class="form-group">
			<label>Nama</label><br>
			<select class="form-control" name="user_id">
				@foreach ( $ulists as $row=>$ulist )
					<option value="{{$ulist->id}}">{{$ulist->id}} ) {{$ulist->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Tanggal Order</label>
			<input class="form-control" placeholder="Tanggal Order" type="date" name="tanggal_order" value="{{old('tanggal_order')}}">
		</div><br>
		
	    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</div>
<!-- /.card-body -->
</form>

@endsection
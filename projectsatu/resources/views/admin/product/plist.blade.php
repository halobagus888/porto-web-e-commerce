@extends('admin/master')

@section('content')
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
         @foreach ($errors->all() as $error)
             <li>{{$error}}</li>
         @endforeach
      </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Produk yang Tersedia</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a style="margin-bottom: 15px;" href="{{url('admin/product/insert')}}" class="btn btn-primary">Insert</a>
                <table id="example2" class="table table-bordered table-hover">
                  <tr>
                    <th style="text-align: center;">KODE BARANG</th>
                    <th style="text-align: center;">KATEGORI</th>
                    <th style="text-align: center;">STOK TERSEDIA</th>
                    <th style="text-align: center;">VARIAN PRODUK</th>
                    <th style="text-align: center;">IMAGE</th>
                    <th style="text-align: center;">EDIT/DELETE</th>
                  </tr>
                  @foreach($plists as $row)
                    <tr>
                      <td><?php echo $row->code; ?></td>
                      <td><?php echo $row->category->nama; ?></td>
                      <td><?php echo $row->stock; ?></td>
                      <td><?php echo $row->varian; ?></td>
                      <td>
                        <a href="{{ asset('img/product/'. $row->image) }}" target="_blank">Lihat Gambar</a>
                        <!-- <img src="{{ asset('img/product'. $row->image) }}" height="10%" width="20%"> -->
                      </td>
                      <td style="text-align: center;">
                        <a class="btn btn-xs btn-primary" href="{{url('admin/product/edit/'.$row->id)}}">Edit</a>
                        <a href="{{url('admin/product/hapus/'.$row->id)}}" class="btn btn-xs btn-danger" onclick="return confirm('Yakin dihapus?');" >Delete</a>
                      </td>
                    </tr>
                  @endforeach
                </table>
                <span>
                  {{ $plists->links() }}
                </span>
                <style>
                  .w-5{display: none;}
                </style>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection
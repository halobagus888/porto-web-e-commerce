@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/product/insert')}}">
	@csrf

	<div class="card-body">
		<div class="form-group">
			<label>Kategori</label><br>
			<select class="form-control" name="category_id">
				@foreach ( $categories as $row )
					<option value="{{$row->id}}">{{$row->nama}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Kode Barang</label>
			<input class="form-control" placeholder="Input Kode Barang (Number)" type="text" name="code" value="{{old('code')}}">
		</div>
		<div class="form-group">
			<label>Nama</label>
			<input class="form-control" placeholder="Nama Barang" type="text" name="name" value="{{old('name')}}">
		</div>
		<div class="form-group">
			<label>Stock</label>
			<input class="form-control" placeholder="Input Stock Tersedia (Number)" type="text" name="stock" value="{{old('stock')}}">
		</div>
		<div class="form-group">
			<label>Varian</label>
			<input class="form-control" placeholder="Varian Barang" type="text" name="varian" value="{{old('varian')}}">
		</div><br>
		<div class="form-group">
			<label>Image</label>
			<input class="form-control" placeholder="Image Barang" type="file" name="image" value="{{old('image')}}">
		</div><br>
		
	    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</div>
<!-- /.card-body -->
</form>

@endsection
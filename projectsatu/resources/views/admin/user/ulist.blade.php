@extends('admin/master')

@section('content')
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
         @foreach ($errors->all() as $error)
             <li>{{$error}}</li>
         @endforeach
      </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Pengguna yang Terdaftar</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a style="margin-bottom: 15px;" href="{{url('admin/user/insert')}}" class="btn btn-primary">Insert</a>
                <table id="example2" class="table table-bordered table-hover">
                  <tr>
                    <th style="text-align: center;">NAMA USER</th>
                    <th style="text-align: center;">DELETE USER</th>
                  </tr>
                  @foreach($ulists as $row)
                    <tr>
                      <td>{{$row['name']}}</td>
                      <td style="text-align: center;">
                        <a href="{{url('admin/user/hapus/'.$row->id)}}" class="btn btn-xs btn-danger" onclick="return confirm('Yakin dihapus?');" >Delete</a>
                      </td>
                    </tr>
                  @endforeach
                </table>
                <span>
                  {{ $ulists->links() }}
                </span>
                <style>
                  .w-5{display: none;}
                </style>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
	</body>
	</html>
@endsection
@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/user/insert')}}">
	@csrf

	<div class="card-body">
		<div class="form-group">
			<label>Nama</label>
			<input class="form-control" placeholder="Nama User" type="text" name="name" value="{{old('name')}}">
		</div>
		<div class="form-group">
	        <label>Email address</label>
	        <input class="form-control" placeholder="Email Address" type="text" name="email" value="{{old('email')}}">
        </div>
        <div class="form-group">
	        <label>Password</label>
	        <input class="form-control" placeholder="Password" type="text" name="password" value="{{old('password')}}">
        </div>
		<br>
		
	    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</div>
<!-- /.card-body -->
</form>

@endsection
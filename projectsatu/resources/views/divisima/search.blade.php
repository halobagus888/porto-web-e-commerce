@extends('divisima.layout')

@section('content')

	<div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
          <tr>
            <th style="text-align: center;">KODE BARANG</th>
            <th style="text-align: center;">KATEGORI</th>
            <th style="text-align: center;">STOK TERSEDIA</th>
            <th style="text-align: center;">VARIAN PRODUK</th>
          </tr>
          @foreach((array) $plists as $row)
            <tr>
              <td><?php echo $row->code; ?></td>
              <td><?php echo $row->category->nama; ?></td>
              <td><?php echo $row->stock; ?></td>
              <td><?php echo $row->varian; ?></td>
            </tr>
          @endforeach
        </table>
        <span>
          {{ $plists->links() }}
        </span>
        <style>
          .w-5{display: none;}
        </style>
    </div>

@endsection